package common

// Tag is a struct for save from JSON information about tag for index
type Tag struct {
	ID           uint32
	CommonIconID uint32
	Tag          string
}
