package sphinx

import (
	"fmt"
	"log"
	"strings"

	"../../common"
	"../../config"
)

// InsertIconToIndex insert new icon to RT index
func InsertIconToIndex(icon common.Icon) {
	var query = fmt.Sprintf("INSERT INTO rt_icons VALUES(%d, '%s', '%s', '%s', %d, %d)", icon.ID, icon.Name, icon.CanonicalName, icon.Description, icon.CommonIconID, icon.PlatformID)
	_, err := config.DBSphinx.Exec(query)
	if err != nil {
		panic(err.Error())
	}
}

// DeleteIconFromIndex delete icon from RT index
func DeleteIconFromIndex(icon common.Icon) {
	// Run delete icon from RT index
	var query = fmt.Sprintf("DELETE FROM rt_icons WHERE id = %d", icon.ID)
	_, err := config.DBSphinx.Exec(query)
	if err != nil {
		panic(err.Error())
	}
}

// SearchIcon run search query by term
func SearchIcon(searchParameters IconSearchQueryParams) []uint32 {
	// Run search using Sphinx
	term := strings.Replace(searchParameters.Term, "'", "\\'", -1)
	var query = fmt.Sprintf("SELECT common_icon_id FROM rt_icons WHERE MATCH('%s') GROUP BY common_icon_id LIMIT %d,%d", term, searchParameters.Offset, searchParameters.Limit)
	rows, err := config.DBSphinx.Query(query)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	// Build result with slice of common_icon_id
	var id uint32
	var result []uint32
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, id)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return result
}

// SearchIconByPlatform run search query by term and platform
func SearchIconByPlatform(searchParameters IconSearchQueryParams) []uint32 {
	// Run search using Sphinx
	term := strings.Replace(searchParameters.Term, "'", "\\'", -1)
	var query = fmt.Sprintf("SELECT common_icon_id FROM rt_icons WHERE MATCH('%s') AND platform_id = %d LIMIT %d,%d", term, searchParameters.PlatformID, searchParameters.Offset, searchParameters.Limit)
	rows, err := config.DBSphinx.Query(query)
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	// Build result with slice of common_icon_id
	var id uint32
	var result []uint32
	for rows.Next() {
		err := rows.Scan(&id)
		if err != nil {
			log.Fatal(err)
		}
		result = append(result, id)
	}
	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}
	return result
}

// InsertTagToIndex insert new icon to RT index
func InsertTagToIndex(tag common.Tag) {
	var query = fmt.Sprintf("INSERT INTO rt_tags VALUES(%d, '%s', '%d')", tag.ID, tag.Tag, tag.CommonIconID)
	_, err := config.DBSphinx.Exec(query)
	if err != nil {
		panic(err.Error())
	}
}

// DeleteTagFromIndex delete icon from RT index
func DeleteTagFromIndex(tag common.Tag) {
	// Run delete icon from RT index
	var query = fmt.Sprintf("DELETE FROM rt_tags WHERE id = %d", tag.ID)
	_, err := config.DBSphinx.Exec(query)
	if err != nil {
		panic(err.Error())
	}
}
