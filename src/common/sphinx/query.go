// Package sphinx contains common structs and functions
// for work with SphinxQL queries
package sphinx

import (
	"log"
	"net/http"
	"strconv"

	"../../config"
)

// IconSearchQueryParams contains parameters for query,
// like term, offset, limit, etc...
type IconSearchQueryParams struct {
	Term       string
	Language   string
	PlatformID uint32
	Offset     uint32
	Limit      uint32
}

// ParseHTTPRequest parse http request and build SearchQuery
func ParseHTTPRequest(request *http.Request) IconSearchQueryParams {
	var searchQueryParams IconSearchQueryParams

	// Parse and assign term
	searchQueryParams.Term = request.URL.Query().Get("term")

	// Parse and assign language
	searchQueryParams.Language = request.URL.Query().Get("language")

	// Parse and assign platformID
	platform := request.URL.Query().Get("platformID")
	platformID, err := strconv.Atoi(platform)
	if err != nil {
		log.Println(err.Error())
		platformID = 0
	}
	searchQueryParams.PlatformID = uint32(platformID)

	// Parse and assign offset
	offset := request.URL.Query().Get("offset")
	offsetNum, err := strconv.Atoi(offset)
	if err != nil {
		offsetNum = 0
	}
	searchQueryParams.Offset = uint32(offsetNum)

	// Parse and assign limit
	limit := request.URL.Query().Get("limit")
	limitNum, err := strconv.Atoi(limit)
	if err != nil {
		limitNum = config.SphinxSearchLimit
	}
	searchQueryParams.Limit = uint32(limitNum)

	return searchQueryParams
}
