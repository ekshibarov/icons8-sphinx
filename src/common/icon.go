package common

// Icon is a struct for save from JSON information about icon for index
type Icon struct {
	ID            uint32
	Name          string
	CanonicalName string
	Description   string
	CommonIconID  uint32
	PlatformID    uint32
}
