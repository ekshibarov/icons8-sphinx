package config

import "database/sql"

// SphinxDSN is DSN string for connect to Sphinx MySQL server
var SphinxDSN = "root:root@tcp(127.0.0.1:9306)/"

// DBSphinx is database connection handle
var DBSphinx *sql.DB

// SphinxSearchLimit is the limit for use in SphinxQL queries
var SphinxSearchLimit = 20
