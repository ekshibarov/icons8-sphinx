package main

import (
	"database/sql"
	"log"
	"net/http"

	"./handle/index"
	"./handle/page"
	"./handle/search"

	"../config"
)

func main() {
	// Init global connection to Sphinx server for run SphinxQL queries
	var err error
	config.DBSphinx, err = sql.Open("mysql", config.SphinxDSN)
	if err != nil {
		panic(err.Error())
	}
	defer config.DBSphinx.Close()

	// Handle HTTP requests
	http.HandleFunc("/", page.Index)                                // set router
	http.HandleFunc("/index/icon/insert", index.InsertIcon)         // handle request to add icon to Sphinx index
	http.HandleFunc("/index/icon/delete", index.DeleteIcon)         // handle request to delete icon from Sphinx index
	http.HandleFunc("/search/icon", search.Icon)                    // handle request for search icon by term using Sphinx
	http.HandleFunc("/search/platform/icon", search.IconByPlatform) // handle request for search icon by term and platform using Sphinx
	log.Println("Start server...")
	err = http.ListenAndServe(":9090", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
