package index

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	// MySQL driver
	_ "github.com/go-sql-driver/mysql"

	"../../../common"
	"../../../common/sphinx"
)

// InsertIcon process request to /index/icon/insert
func InsertIcon(w http.ResponseWriter, request *http.Request) {
	printMessage(w)
	sphinx.InsertIconToIndex(decodeIcon(request))
}

// DeleteIcon process request to /index/icon/delete
func DeleteIcon(w http.ResponseWriter, request *http.Request) {
	printMessage(w)
	sphinx.DeleteIconFromIndex(decodeIcon(request))
}

// printMessage print message to ResponseWriter
func printMessage(w http.ResponseWriter) {
	message := `
	Add icon to RT index.
	Use POST request for send JSON like {"name": "icons8_logo", "canonicalName": "icons8", "description": "This is an Icons8 Logo", "commonIconID": 1}
	`
	fmt.Fprintf(w, message) // send data to client side
}

// decodeIcon try to decode JSON to Icon struct
func decodeIcon(request *http.Request) common.Icon {
	request.ParseForm()
	decoder := json.NewDecoder(request.Body)
	var icon common.Icon
	err := decoder.Decode(&icon)
	if err != nil {
		log.Println("Invalid JSON")
	}
	defer request.Body.Close()
	return icon
}
