package page

import (
	"fmt"
	"net/http"
)

// Index process request to /
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Icons8 Sphinx RT index updater") // send data to client side
}
