package search

import (
	"net/http"

	"encoding/json"
	"log"

	"../../../common/sphinx"
)

// IconData struct contains info about founded icon
type IconData struct {
	CommonIconID uint32
}

// IconDataSlice struct contains list of IconData
type IconDataSlice struct {
	Icons []IconData
}

// Icon handle request for search icon by term using Sphinx
func Icon(w http.ResponseWriter, request *http.Request) {
	searchParameters := sphinx.ParseHTTPRequest(request)
	var searchResult []uint32
	searchResult = sphinx.SearchIcon(searchParameters)
	iconsJSON := buildResponse(searchResult)
	w.Header().Set("Content-Type", "application/json")
	w.Write(iconsJSON)
}

// IconByPlatform handle requst for search icon by term and platform using Sphinx
func IconByPlatform(w http.ResponseWriter, request *http.Request) {
	searchParameters := sphinx.ParseHTTPRequest(request)
	var searchResult []uint32
	searchResult = sphinx.SearchIconByPlatform(searchParameters)
	iconsJSON := buildResponse(searchResult)
	w.Header().Set("Content-Type", "application/json")
	w.Write(iconsJSON)
}

// buildResponse build JSON response
func buildResponse(icons []uint32) []byte {
	var iconsData IconDataSlice
	for _, ID := range icons {
		iconsData.Icons = append(iconsData.Icons, IconData{CommonIconID: ID})
	}
	iconsJSON, err := json.Marshal(iconsData)
	if err != nil {
		log.Println(err.Error())
	}
	return iconsJSON
}
