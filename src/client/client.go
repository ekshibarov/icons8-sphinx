package main

import (
	"database/sql"
	"strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"

	"../common"
	"../common/sphinx"
	"../config"
)

func main() {
	// Init global connection to Sphinx server for run SphinxQL queries
	var err error
	config.DBSphinx, err = sql.Open("mysql", config.SphinxDSN)
	if err != nil {
		panic(err.Error())
	}
	defer config.DBSphinx.Close()
	// loadIcons()
	loadTags()
}

func loadIcons() {
	// Open database connection
	db, err := sql.Open("mysql", "root:root@/icons8")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	// Execute the query
	query := `
	SELECT
		ci.common_icon_id, ci.name, ci.canonical_name, ci.description, i.icon_id, i.platform_id FROM icon i
	JOIN common_icon ci ON ci.common_icon_id = i.common_icon_id
	WHERE i.status='published'
	`
	rows, err := db.Query(query)
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Make a slice for the values
	values := make([]sql.RawBytes, len(columns))

	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	// Fetch rows
	for rows.Next() {
		// get RawBytes from data
		err = rows.Scan(scanArgs...)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		// Now do something with the data.
		// Here we just print each column as a string and create
		var icon common.Icon
		for i, col := range values {
			// Here we can check if the value is nil (NULL value)
			if col != nil {
				switch columns[i] {
				case "icon_id":
					value, err := strconv.Atoi(string(col))
					if err != nil {
						panic(err.Error())
					}
					icon.ID = uint32(value)
					continue
				case "common_icon_id":
					value, err := strconv.Atoi(string(col))
					if err != nil {
						panic(err.Error())
					}
					icon.CommonIconID = uint32(value)
					continue
				case "name":
					icon.Name = strings.Replace(string(col), "'", "\\'", -1)
					continue
				case "canonical_name":
					icon.CanonicalName = strings.Replace(string(col), "'", "\\'", -1)
					continue
				case "description":
					icon.Description = strings.Replace(string(col), "'", "\\'", -1)
					continue
				case "platform_id":
					value, err := strconv.Atoi(string(col))
					if err != nil {
						panic(err.Error())
					}
					icon.PlatformID = uint32(value)
					continue
				}
			}
		}
		sphinx.InsertIconToIndex(icon)
	}
	if err = rows.Err(); err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
}

func loadTags() {
	// Open database connection
	db, err := sql.Open("mysql", "root:root@/icons8")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	// Reset number of
	db.Exec("SET @icon_tag_id=0")

	// Execute the query
	query := `
	SELECT @icon_tag_id:=@icon_tag_id+1 AS icon_tag_id, it.common_icon_id, t.tag FROM icon_tag it
	JOIN tag t on t.tag_id = it.tag_id
	`
	rows, err := db.Query(query)
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Get column names
	columns, err := rows.Columns()
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	// Make a slice for the values
	values := make([]sql.RawBytes, len(columns))

	// rows.Scan wants '[]interface{}' as an argument, so we must copy the
	// references into such a slice
	// See http://code.google.com/p/go-wiki/wiki/InterfaceSlice for details
	scanArgs := make([]interface{}, len(values))
	for i := range values {
		scanArgs[i] = &values[i]
	}

	// Fetch rows
	for rows.Next() {
		// get RawBytes from data
		err = rows.Scan(scanArgs...)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		// Now do something with the data.
		// Here we just print each column as a string and create
		var tag common.Tag
		for i, col := range values {
			// Here we can check if the value is nil (NULL value)
			if col != nil {
				switch columns[i] {
				case "icon_tag_id":
					value, err := strconv.Atoi(string(col))
					if err != nil {
						panic(err.Error())
					}
					tag.ID = uint32(value)
					continue
				case "common_icon_id":
					value, err := strconv.Atoi(string(col))
					if err != nil {
						panic(err.Error())
					}
					tag.CommonIconID = uint32(value)
					continue
				case "tag":
					tag.Tag = strings.Replace(string(col), "'", "\\'", -1)
					continue
				}
			}
		}
		sphinx.InsertTagToIndex(tag)
	}
	if err = rows.Err(); err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
}
