#!/usr/bin/env bash

# Using for Ubuntu for install software dependencies

# Install MySQL server
sudo apt-get install mysql-server

# Install latest Sphinx 2.2.x
sudo add-apt-repository ppa:builds/sphinxsearch-rel22 -y
sudo apt-get update
sudo apt-get install sphinxsearch

# Install Git
sudo apt-get install git

# Install Golang 1.7.4
wget https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz
sudo tar -zxvf go1.7.4.linux-amd64.tar.gz -C /usr/local/
echo 'export GOROOT=/usr/local/go' >> ~/.bashrc
echo 'export GOPATH=$HOME/go' >> ~/.bashrc
echo 'export PATH=$PATH:$GOROOT/bin:$GOPATH/bin' >> ~/.bashrc
rm go1.7.4.linux-amd64.tar.gz
