#!/usr/bin/env bash

DB_USER="root"
DB_PASSWORD="root"
DB_NAME="icons8";
ICONS_DATA_FILE="icons_data.sql"

mysql -u$DB_USER -p$DB_PASSWORD < init.sql
if [ -f $ICONS_DATA_FILE ];
then
    mysql -u$DB_USER -p$DB_PASSWORD $DB_NAME < icons_data.sql
fi
