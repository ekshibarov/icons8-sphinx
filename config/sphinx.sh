#!/usr/bin/env bash

sudo service sphinxsearch stop
sudo cp sphinx.conf /etc/sphinxsearch/sphinx.conf
sudo service sphinxsearch start
